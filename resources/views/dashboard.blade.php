<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div class="m-5 p-5 border">
                        <h4>Process 1</h4>
                        <label for="main_data">File for exports_items.csv</label>
                        <br>
                        <input type="file" accept=".csv" name="main_data" id="mainData">
                        <input type="text" placeholder="Supplier Name" name="supplier_name_1" id="supplierName1">
                        <button onclick="process1()" id="process1" class="p-2 border bg-blue-100">Process</button>
                    </div>

                    <div class="m-5 p-5 border">
                        <h4>Process 2</h4>
                        <label for="supplier_po_data">File for SupplierNamePO.csv</label>
                        <br>
                        <input type="file" accept=".csv" name="supplier_po_data" id="supplierPOData">
                        <input type="text" placeholder="Supplier Name" name="supplier_name_2" id="supplierName2">
                        <button onclick="process2()" id="process2" class="p-2 border bg-blue-100">Process</button>
                    </div>

                    <div class="m-5 p-5 border">
                        <h4>Process 3</h4>
                        <label for="store_po_data">File for LocationNameSupplierNamePO.csv</label>
                        <br>
                        <input type="file" accept=".csv" name="store_po_data" id="storePOData">
                        <input type="text" placeholder="Supplier Name" name="supplier_name_3" id="supplierName3">
                        <button onclick="process3()" id="process3" class="p-2 border bg-blue-100">Process</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/js/helpers.js"></script>
    <script>
        // mainData[0] is header
        // mainData[?][0] is handle
        // mainData[?][1] is sku
        // mainData[?][2] is name
        // mainData[?][4] is description
        // mainData[?][7] is "Option 1 value"
        // mainData[?][14] is barcode
        // mainData[?][19] is supplier name
        // mainData[?][20] is the last field "Purchase Cost" before locations

        document.addEventListener("DOMContentLoaded", () => {
            initialize()
        })

        let mainDataElement = document.querySelector('#mainData')
        let supplierPODataElement = document.querySelector('#supplierPOData')
        let storePODataElement = document.querySelector('#storePOData')

        let supplierName1Element = document.querySelector('#supplierName1')
        let supplierName2Element = document.querySelector('#supplierName2')
        let supplierName3Element = document.querySelector('#supplierName3')

        let mainData = []
        let mainDataCopy = []
        let supplierNamePOData = []
        let supplierNamePOExternalData = []
        let purchaseOrdersData = []
        let isAPI = true

        let stores = []
        let headers = []

        supplierName1Element.addEventListener('input', event => {
            supplierName2Element.value = event.target.value
            supplierName3Element.value = event.target.value
        })
    </script>
    <script src="/js/first_process.js"></script>
    <script src="/js/second_process.js"></script>
    <script src="/js/third_process.js"></script>
</x-app-layout>
