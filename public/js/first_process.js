mainDataElement.addEventListener('change', (event) => {
    clearData()
    readCSVFile(event.target.files[0], 'mainData')
    isAPI = false
})

function process1() {
    headers = mainData.shift()
    let supplierName = supplierName1Element.value

    // Get all store names
    headers.forEach(header => {
        if (header.includes('Available for sale')) {
            let store = header.split('[')
            store = store[1]
            store = store.substr(0, store.length - 1)
            stores.push(store)
        }
    })

    console.log('stores', stores)

    // Filter main data by supplier name, get handles only
    let mainDataFilteredBySupplierName = mainData.filter(row => {
        return row[19] == supplierName
    }).map(row => row[0])

    // Filter main data by handle
    let mainDataFilteredByHandle = mainData.filter(row => {
        for (const subRow of mainDataFilteredBySupplierName) {
            if (subRow == row[0]) {
                return true
            }
        }
    })

    commonProcess(mainDataFilteredByHandle, false, supplierName)
}