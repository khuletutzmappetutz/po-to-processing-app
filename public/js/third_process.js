storePODataElement.addEventListener('change', event => {
    clearData()
    readCSVFile(event.target.files[0], purchaseOrdersData)
})

function process3() {
    supplierName = supplierName3Element.value

    // Remove purchase cost column
    purchaseOrdersData = purchaseOrdersData.map(row => {
        return row.filter((col, index) => {
            return index != 5
        })
    })

    createCSVFile(purchaseOrdersData, supplierName + 'TO')
}