function initialize() {
    fetchMainData()
    fetchStores
}

function fetchMainData() {
    const xhr = new XMLHttpRequest()

    xhr.onload = (response) => {
        console.log(response)
        mainData = response.items
    }

    xhr.open('GET', 'https://api.loyverse.com/v1.0/items')
    xhr.setRequestHeader('Content-Type', 'application/json')
    xhr.setRequestHeader('Authorization', 'Bearer 77073fd944f44ad098f14ec5268baef8')
    xhr.send()
}

function fetchStores() {
    const xhr = new XMLHttpRequest()

    xhr.onload = (response) => {
        console.log(response)
        stores = response.stores
    }

    xhr.open('GET', 'https://api.loyverse.com/v1.0/stores')
    xhr.setRequestHeader('Content-Type', 'application/json')
    xhr.setRequestHeader('Authorization', 'Bearer 77073fd944f44ad098f14ec5268baef8')
    xhr.send()
}

function readCSVFile(file, container) {
    let data = []
    const fileReader = new FileReader()

    fileReader.readAsText(file)
    fileReader.onload = (event) => {
        const csv = event.target.result
        const rows = csv.split('\n')

        data = rows.map(row => {
            return row.split(',')
        })

        switch (container) {
            case 'mainData':
                mainData = data
                mainDataCopy = mainData.slice()
                break;

            case 'supplierNamePOExternalData':
                supplierNamePOExternalData = data

            default:
                purchaseOrdersData = data
                break;
        }
    }
}

function createCSVFile(data, fileName) {
    data = data
        .map(row => row.join(","))
        .join("\n")

    const blob = new Blob([data], { type: 'text/csv' })
    const url = window.URL.createObjectURL(blob)
    const a = document.createElement('a')

    a.setAttribute('href', url)
    a.setAttribute('download', fileName + '.csv');
    a.click()
}

function commonProcess(data, isRefined = false, supplierName) {
    for (const store of stores) {
        console.log('store', store)

        // Get store header positions
        availableForSalePosition = null
        pricePosition = null
        inStockPosition = null
        lowStockPosition = null
        optimalStockPosition = null

        headers.forEach((header, index) => {
            console.log('header', header)
            console.log('index', index)
            console.log('store', store)

            if (header == 'Available for sale [' + store + ']') {
                console.log('matched')

                availableForSalePosition = index
                pricePosition = index + 1
                inStockPosition = index + 2
                lowStockPosition = index + 3
                optimalStockPosition = index + 4

                return
            }
        })

        console.log('availableForSalePosition', availableForSalePosition)
        console.log('pricePosition', pricePosition)
        console.log('inStockPosition', inStockPosition)
        console.log('lowStockPosition', lowStockPosition)
        console.log('optimalStockPosition', optimalStockPosition)

        // Setting store PO headers
        let locationNameSupplierNamePOData = [
            [
                'SKU',
                'Item name',
                'Variant name',
                'Barcode',
                'Quantity',
                'Purchase cost',
                'Amount',
            ]
        ]

        for (const row of data) {
            console.log('row', row)
            console.log('row[1]', row[1])

            availableForSale = row[availableForSalePosition]
            price = Number(row[pricePosition])
            inStock = Number(row[inStockPosition])
            lowStock = Number(row[lowStockPosition])
            optimalStock = Number(row[optimalStockPosition])
            description = row[4]

            console.log('availableForSale', availableForSale)
            console.log('price', price)
            console.log('inStock', inStock)
            console.log('lowStock', lowStock)
            console.log('optimalStock', optimalStock)
            console.log('description', description)

            // Check for valid values
            if (inStock != 'variable' && lowStock != 'variable' && optimalStock != 'variable') {
                // Skip records with no "low_stock" or "optimal_stock" value
                if (typeof (lowStock) != "number" || typeof (optimalStock) != "number") {
                    continue
                }

                if (inStock <= lowStock) {
                    let order = optimalStock - inStock
                    console.log(order)

                    if (description) {
                        // Parse description with html entities
                        let tempDivElement = document.createElement('div')
                        tempDivElement.innerHTML = description
                        description = tempDivElement.innerText
                        tempDivElement = null

                        order += (description - (order % description))
                    }

                    // Filter zeroes and negatives
                    if (order <= 0) {
                        continue
                    }

                    // sku, item_name, variant_name, barcode, quantity, purchase_cost, amount
                    locationNameSupplierNamePOData.push([
                        row[1],
                        row[2],
                        row[7],
                        row[14],
                        order,
                        0,
                        0
                    ])
                }
            }
        }

        // Add to array for aggregation
        supplierNamePOData.push(locationNameSupplierNamePOData)
        createCSVFile(locationNameSupplierNamePOData, store + supplierName + 'PO')
    }

    // Deduplicate array
    supplierNamePOData = supplierNamePOData.flat()
    supplierNamePOData = [...new Set(supplierNamePOData)]

    // Remove all PO headers
    supplierNamePOData = supplierNamePOData.filter(row => {
        return row[0] != 'SKU' && row[1] != 'Item name'
    })

    // Duplicate store PO
    let supplierNamePODataCopy = supplierNamePOData.slice()

    // Aggregate quantities from store POs
    supplierNamePOData = supplierNamePOData.map(row => {
        total = 0

        supplierNamePODataCopy.forEach(subRow => {
            if (row[0] == subRow[0]) {
                total += subRow[4]
            }
        })

        row[4] = total
        return row
    })

    // Add one PO headers
    supplierNamePOData.unshift([
        [
            'SKU',
            'Item name',
            'Variant name',
            'Barcode',
            'Quantity',
            'Purchase cost',
            'Amount',
        ]
    ])

    if (isRefined) {
        createCSVFile(supplierNamePOData, supplierName + 'PO_RF')
    } else {
        createCSVFile(supplierNamePOData, supplierName + 'PO')
    }
}

function clearData() {
    supplierNamePOData = []
    supplierNamePOExternalData = []
    purchaseOrdersData = []
}