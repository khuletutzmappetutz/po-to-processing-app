supplierPODataElement.addEventListener('change', event => {
    clearData()
    readCSVFile(event.target.files[0], 'supplierNamePOExternalData')
    isAPI = false
})

// Needs main_data to have value
function process2() {
    let supplierName = supplierName2Element.value

    // Filter main data by skus
    let mainDataFilteredBySKU = mainData.filter(row => {
        for (const subRow of supplierNamePOExternalData) {
            if (subRow[0] == row[1]) {
                return true
            }
        }
    })

    commonProcess(mainDataFilteredBySKU, true, supplierName)
}